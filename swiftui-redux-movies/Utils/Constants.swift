//
//  Constants.swift
//  MoviesApp
//
//  Created by JP on 24-08-23.
//

import Foundation

struct Constants {

    struct ApiKeys {
        static let omdbIdKey = "API KEY"
    }

    struct Urls {
        static func urlBySearch(search: String) -> String {
            "http://www.omdbapi.com/?s=\(search)&page=1&apikey=\(ApiKeys.omdbIdKey)"
        }

        static func urlForMovieDetailsByImdbId(imdbId: String) -> String {
            "http://www.omdbapi.com/?i=\(imdbId)&apikey=\(ApiKeys.omdbIdKey)"
        }
    }
}
