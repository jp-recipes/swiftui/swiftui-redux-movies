//
//  swiftui_redux_moviesApp.swift
//  swiftui-redux-movies
//
//  Created by JP on 24-08-23.
//

import SwiftUI

@main
struct MoviesReduxApp: App {
    var body: some Scene {
        let store = Store(reducer: appReducer, state: AppState(), middlewares: [
            moviesMiddleware()
        ])

        WindowGroup {
            ContentView().environmentObject(store)
        }
    }
}
