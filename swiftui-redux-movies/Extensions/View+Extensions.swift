//
//  View+Extensions.swift
//  MoviesApp
//
//  Created by JP on 24-08-23.
//

import Foundation
import SwiftUI

extension View {

    func embedInNavigationView() -> some View {
        NavigationView { self }
    }

}
