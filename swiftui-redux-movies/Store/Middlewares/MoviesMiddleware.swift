//
//  MoviesMiddleware.swift
//  swiftui-redux-movies
//
//  Created by JP on 24-08-23.
//

import Foundation

func moviesMiddleware() -> Middleware<AppState> {
    return { state, action, dispatch in
        switch action {
        case let action as FetchMovies:
            Webservice().getMoviesBy(search: action.search.urlEncode()) { result in
                switch result {
                case .success(let movies):
                    dispatch(SetMovies(movies: movies))
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        case let action as FetchMovieDetails:
            Webservice().getMovieDetailsBy(imdbId: action.imdbId) { result in
                switch result {
                case .success(let details):
                    dispatch(SetMovieDetails(details: details))
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        default:
            break
        }
    }
}
